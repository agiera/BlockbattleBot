This is an ai that plays blockbattle. 
I made up my own algorithm to explore properties of neural networks.

It simulates playing tetris without an opponent to find optimal moves.
This creates an exponential amount of outcomes. I calulated the amount of
competative moves to be around 81 which is a huge branching factor.
So what I did was use a neural network to prune the decision branches. 
I used an epsilon-greedy to choose the neural nets k favorite decisions.
The theory is that I can start out exploring the whole decision tree and 
as the neural network grows I can down the branching factor and be able
to search the relavent parts of the tree faster. Over the summer I want 
to try to create a more modular package for this algorithm and test it
against different pruning algorithms like alpha-beta or maybe try to use 
them with it.








